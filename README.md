# Information / Информация

Интеграция значков в статью.

## Install / Установка

1. Загрузите папки и файлы в директорию `extensions/MW_EXT_Badge`.
2. В самый низ файла `LocalSettings.php` добавьте строку:

```php
wfLoadExtension( 'MW_EXT_Badge' );
```

## Syntax / Синтаксис

```html
{{#badge: user|music}}
```

## Donations / Пожертвования

- [Donation Form](https://donation-form.github.io/)
